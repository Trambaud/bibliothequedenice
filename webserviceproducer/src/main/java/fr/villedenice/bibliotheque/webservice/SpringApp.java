package fr.villedenice.bibliotheque.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "fr.villedenice.bibliotheque"})
@EnableJpaRepositories(basePackages = { "fr.villedenice.bibliotheque.consumer"})
@EntityScan(basePackages = { "fr.villedenice.bibliotheque.model" })
public class SpringApp {
	public static void main(String[] args) {
		SpringApplication.run(SpringApp.class, args);
	}
}