package fr.villedenice.bibliotheque.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.villedenice.bibliotheque.consumer.CustomerRepository;
import fr.villedenice.bibliotheque.model.Borrow;
import fr.villedenice.bibliotheque.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public List<Borrow> getCustomerBorrowHistory(Integer customerId) {
		Customer customer = customerRepository.findByCustomerId(customerId);
		List<Borrow> borrowList = customer.getBorrow();
		return borrowList;
	}

	@Override
	public boolean logIn(String mail, String password) {
		
		Customer obj = customerRepository.findByMail(mail);
		if (obj.getPassword().equals(password)) {
			return true;
		}
		return false;
	}
	
	@Override
	public void logOut() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Customer findByMail(String mail) {
		Customer obj = customerRepository.findByMail(mail);
		return obj;
	}

	@Override
	public Customer findByCustomerId(Integer customerId) {
		Customer customer = customerRepository.findByCustomerId(customerId);
		return customer;
	}

}
