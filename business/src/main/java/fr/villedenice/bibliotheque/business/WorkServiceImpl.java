package fr.villedenice.bibliotheque.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.villedenice.bibliotheque.consumer.BookRepository;
import fr.villedenice.bibliotheque.consumer.WorkRepository;
import fr.villedenice.bibliotheque.model.Book;
import fr.villedenice.bibliotheque.model.Work;

@Service
public class WorkServiceImpl implements WorkService {
	@Autowired
	private WorkRepository workRepository;
	@Autowired
	private BookRepository bookRepository;

	@Override
	public List<Work> findByAuthor(String authorName) {
		List<Work> obj = workRepository.findByAuthorContainsIgnoreCase(authorName);
		return obj;

	}

	@Override
	public List<Work> findByTitle(String workTitle) {
		List<Work> obj = workRepository.findByTitleContainsIgnoreCase(workTitle);
		return obj;
	}

	@Override
	public List<Work> findByYearPublication(int yearPublication) {
		List<Work> obj = workRepository.findByYearPublicationGreaterThanEqual(yearPublication);
		return obj;
	}

	@Override
	public List<Work> getAllWork() {
		List<Work> list = new ArrayList<>();
		workRepository.findAll().forEach(e -> list.add(e));
		return list;
	}

	@Override
	public boolean addWork(Work work) {

		List<Work> list = workRepository.findByTitleContainsIgnoreCase(work.getTitle());
		if (list.size() > 0) {
			return false;
		} else {

			
			List<Book> books = new ArrayList<>();
			
			for (int i = work.getStockQuantity(); i > 0; i--) {
				Book book = new Book();
				book.setAvailable(true);
				book.setReference(work.getTitle());
				
				bookRepository.save(book);
				
				books.add(book);

			}
			work.setBooks(books);
			workRepository.save(work);
		

			return true;
		}
	}

	@Override
	public void updateWork(Work work) {
		workRepository.save(work);

	}

	@Override
	public Work findByWorkId(int workId) {
		Work work = workRepository.findByWorkId(workId);
		return work;
	}

	@Override
	public List<Work> findByFilter(String author, String title, Integer year) {
		//TODO algorithme
		List<Work> workList = new ArrayList<>();
		//Cas : Titre et année
		if(author.isEmpty() && !title.isEmpty() && !year.toString().isEmpty()) {
			workList = workRepository.findByTitleIgnoreCaseContainsAndYearPublicationGreaterThanEqual(title, year);
		}
		//Cas : Auteur et année
		else if (title.isEmpty()&& !author.isEmpty() && !year.toString().isEmpty()) {
			workList = workRepository.findByAuthorIgnoreCaseContainsAndYearPublicationGreaterThanEqual(author, year);
		}
		//Cas : Auteur et titre
		else if (year.toString().isEmpty() && !title.isEmpty() && !author.isEmpty()) {
			workList = workRepository.findByAuthorIgnoreCaseContainsAndTitleIgnoreCaseContains(author, title);
		}
		//Cas : Auteur uniquement
		else if (!author.isEmpty() && title.isEmpty() && year.toString().isEmpty()) {
			workList = workRepository.findByAuthorContainsIgnoreCase(author);
		}
		//Cas : Titre uniquement
		else if (author.isEmpty() && !title.isEmpty() && year.toString().isEmpty()) {
			workList = workRepository.findByTitleContainsIgnoreCase(title);
		}
		//Cas : Année uniquement
		else if (author.isEmpty() && title.isEmpty() && !year.toString().isEmpty()) {
			workList = workRepository.findByYearPublicationGreaterThanEqual(year);
		}
		else if (!author.isEmpty() && !title.isEmpty() && !year.toString().isEmpty()) {
			workList = workRepository.findByAuthorIgnoreCaseContainsAndTitleIgnoreCaseContainsAndYearPublicationGreaterThanEqual(author, title, year);
		}
		else {
			workList = getAllWork();
		}
		return workList;
	}


}
