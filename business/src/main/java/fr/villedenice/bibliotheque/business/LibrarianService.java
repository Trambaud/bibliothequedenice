package fr.villedenice.bibliotheque.business;

import java.util.List;

import fr.villedenice.bibliotheque.model.*;

public interface LibrarianService {
	public List<Borrow> getCustomerCurrentBorrows(Customer customer);

	public List<Borrow> getAllBorrow();

	public Book addBook(Work work);
	public Book removeBook(Book book);

}
