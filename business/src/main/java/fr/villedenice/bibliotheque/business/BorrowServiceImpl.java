package fr.villedenice.bibliotheque.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.villedenice.bibliotheque.consumer.BookRepository;
import fr.villedenice.bibliotheque.consumer.BorrowRepository;
import fr.villedenice.bibliotheque.consumer.CustomerRepository;
import fr.villedenice.bibliotheque.consumer.WorkRepository;
import fr.villedenice.bibliotheque.model.Book;
import fr.villedenice.bibliotheque.model.Borrow;
import fr.villedenice.bibliotheque.model.Customer;
import fr.villedenice.bibliotheque.model.Work;

@Service
public class BorrowServiceImpl implements BorrowService {
	@Autowired
	private WorkRepository workRepository;
	@Autowired
	private BorrowRepository borrowRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private BookRepository bookRepository;

	@Override
	public boolean startBorrow(Integer workId, Integer customerId) {
		boolean result = false;
		// Recuperer l'oeuvre � partir du workId --> workGot
		Work work = workRepository.findByWorkId(workId);
		Customer customer = customerRepository.findByCustomerId(customerId);
		// Recuperer l'ensemble des book de workGot --> bookList
		List<Book> bookList = work.getBooks();
		// Parcourir bookList --> for
		for (int i = 0; i < bookList.size(); i++) {
			Book book = bookList.get(i);
			if (book.isAvailable() == true) {

				Borrow borrow = new Borrow();
				borrow.setBorrowDate(new Date());
				Calendar limitDate = Calendar.getInstance();
				limitDate.add(Calendar.DATE, 28);
				borrow.setLimitDate(limitDate.getTime());
				borrow.setStatus(1);
				borrow.setCustomer(customer);
				borrow.setBook(book);
				borrowRepository.save(borrow);
				book.setAvailable(false);
				book.setBorrow(borrow);
				bookRepository.save(book);
				List<Borrow> customerBorrow = customer.getBorrow();
				customerBorrow.add(borrow);
				customer.setBorrow(customerBorrow);
				customerRepository.save(customer);
				result = true;

				break;
			}

		}
		// Pour chaque bookList --> book
		// if status book == 2
		// cr�er un emprunt (new Borrow())
		// lie le customer au borrow cr�e
		// lie le book au borrow cr�e
		// informations annexes --> date, status
		// sauvegarder en base borrow --> borrowtoreturn
		// break;

		// return borrowtoreturn

		return result;
	}

	public void extendBorrow(Borrow borrow) {
		// Récuperer l'emprunt à partir du borrowId
		// Borrow borrow = borrowRepository.findByBorrowId(borrowId);
		// Recuperer le statut de l'emprunt --> status
		int status = borrow.getStatus();
		// if status == 1
		if (status == 1) {
			status = 3;
			borrow.setStatus(status);
			Calendar endDate = Calendar.getInstance();
			endDate.setTime(borrow.getLimitDate());
			endDate.add(Calendar.DATE, 28);
			borrow.setLimitDate(endDate.getTime());
			borrowRepository.save(borrow);
		}
		// changer le status --> 3
		// prolonger la date de restitution --> +4 semaines (Calendar)
		// update en base borrow

	}

	@Override
	public void endBorrow(Borrow borrow) {

		Book book = bookRepository.findByBorrow(borrow);
		Calendar limitDate = Calendar.getInstance();
		borrow.setLimitDate(limitDate.getTime());
		borrow.setStatus(2);
		borrowRepository.save(borrow);
		book.setAvailable(true);
		book.setBorrow(null);
		bookRepository.save(book);

	}

	@Override
	public Borrow getBorrowById(Integer borrowId) {
		Borrow borrow = borrowRepository.findByBorrowId(borrowId);
		return borrow;
	}

	@Override
	public List<Borrow> getAllBorrow(Customer customer) {

		List<Borrow> list = borrowRepository.findByCustomer(customer);
		return list;

	}

	@Override
	public void sendMail() {

		List<Borrow> borrowList = new ArrayList<>();
		borrowRepository.findAll().forEach(e -> borrowList.add(e));
		Date today = new Date();
		String username = "bibliothequedenice@gmail.com";
		String password = "bdn123456";

		// 1 -> Création de la session
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		for (Borrow borrow : borrowList) {
			Date limitDate = borrow.getLimitDate();
			if (today.compareTo(limitDate) >= 0 && borrow.getStatus() != 2) {
				// 2. Creation du message

				try {
					MimeMessage message = new MimeMessage(session);
					message.setText("Bonjour" + borrow.getCustomer().getFirstName() + ", l'emprunt pour le livre : "
							+ borrow.getBook().getReference()
							+ " est terminé. N'oubliez pas de le retourner afin d'éviter toute pénalités. Vous pouvez également prolonger votre emprunt, sauf si vous l'avez déjà prolongé une fois.");

					message.setSubject("Bibliothèque de Nice : emprunt terminé");
					message.setFrom(new InternetAddress(username));
					 InternetAddress[] address = {new InternetAddress(borrow.getCustomer().getMail())};
					message.setRecipients(Message.RecipientType.TO, address);

					// 3. Envoi du message
					Transport.send(message);

				} catch (MessagingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		}

	}

}
