package fr.villedenice.bibliotheque.business;

import org.springframework.beans.factory.annotation.Autowired;

import fr.villedenice.bibliotheque.consumer.BookRepository;
import fr.villedenice.bibliotheque.model.Book;

public class BookServiceImpl implements BookService {
	@Autowired
	private BookRepository bookRepository;

	@Override
	public Book getByBookId(Integer bookId) {
		Book book = bookRepository.findByBookId(bookId);
		return book;

	}

}
