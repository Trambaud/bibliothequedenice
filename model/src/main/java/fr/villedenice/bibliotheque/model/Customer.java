package fr.villedenice.bibliotheque.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity(name = "customer")
@Table(name = "customer")
@Inheritance(strategy = InheritanceType.JOINED)
public class Customer implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected int customerId;
	@Column(name="first_name")
	protected String firstName;
	@Column(name="last_name")
	protected String lastName;
	protected String address;
	protected String mail;
	protected String password;

	@OneToMany(fetch=FetchType.EAGER)
	private List<Borrow> borrow;

	

	public Customer(String first_name, String last_name, String address, String mail, String password) {
		super();
		this.firstName = first_name;
		this.lastName = last_name;
		this.address = address;
		this.mail = mail;
		this.password=password;
		
	}

	public Customer() {
		
	}
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Borrow> getBorrow() {
		return borrow;
	}

	public void setBorrow(List<Borrow> borrow) {
		this.borrow = borrow;
	}
	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
