package fr.villedenice.bibliotheque.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity(name="borrow")
@Table(name="borrow")
public class Borrow implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	protected int borrowId;
	@Column(name="borrow_date")
	@Temporal(TemporalType.DATE)
	protected Date borrowDate;
	@Column(name="limit_date")
	@Temporal(TemporalType.DATE)
	protected Date limitDate;
	@Column(name="status")
	protected int status;// 1: en cours 2 : rendu 3 : prolong�
	@OneToOne
	private Book book;
	@ManyToOne(fetch=FetchType.EAGER)
	private Customer customer;
	
	public Borrow() {

	}
	
	public Borrow(int id, Date borrowDate, Date limitDate, int status) {
		super();
		this.borrowId = id;
		this.borrowDate = borrowDate;
		this.limitDate = limitDate;
		this.status = status;
	}

	public int getBorrowId() {
		return borrowId;
	}

	public void setBorrowId(int borrowId) {
		this.borrowId = borrowId;
	}

	public Date getBorrowDate() {
		return borrowDate;
	}

	public void setBorrowDate(Date borrowDate) {
		this.borrowDate = borrowDate;
	}

	public Date getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(Date limitDate) {
		this.limitDate = limitDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}




}
