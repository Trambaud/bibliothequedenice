package fr.villedenice.bibliotheque.client.test;

import java.util.List;

import fr.villedenice.bibliotheque.client.ws.AddWorkRequest;
import fr.villedenice.bibliotheque.client.ws.AddWorkResponse;
import fr.villedenice.bibliotheque.client.ws.BookInfo;
import fr.villedenice.bibliotheque.client.ws.GetAllWorkRequest;
import fr.villedenice.bibliotheque.client.ws.GetAllWorkResponse;
import fr.villedenice.bibliotheque.client.ws.GetWorkByAuthorRequest;
import fr.villedenice.bibliotheque.client.ws.GetWorkByAuthorResponse;
import fr.villedenice.bibliotheque.client.ws.GetWorkByDateRequest;
import fr.villedenice.bibliotheque.client.ws.GetWorkByDateResponse;
import fr.villedenice.bibliotheque.client.ws.GetWorkByTitleRequest;
import fr.villedenice.bibliotheque.client.ws.GetWorkByTitleResponse;
import fr.villedenice.bibliotheque.client.ws.WorkInfo;
import fr.villedenice.bibliotheque.client.ws.WorkPort;
import fr.villedenice.bibliotheque.client.ws.WorkPortService;

public class WorkServiceTest {

	public static void main(String[] args) {

		WorkPortService workPortService = new WorkPortService();
		WorkPort workPort = workPortService.getWorkPortSoap11();
		// GETWORKBYAUTHOR -->
		GetWorkByAuthorRequest author = new GetWorkByAuthorRequest();
		author.setAuthor("Roald Dahl");
		GetWorkByAuthorResponse authorResponse = workPort.getWorkByAuthor(author);
		List<WorkInfo> workListByAuthor = authorResponse.getWorkInfo();
		System.out.println("-----GetWorkByAuthor-----");
		for (int i = 0; i < workListByAuthor.size(); i++) {
			System.out.println("Titre : " + workListByAuthor.get(i).getTitle() + "\nAuteur : "
					+ workListByAuthor.get(i).getAuthor() + "\nR�sum� : " + workListByAuthor.get(i).getSummary()
					+ "\nAnn�e de parution : " + workListByAuthor.get(i).getYearPublication());
		}
		// GETWORKBYTITLE -->
		GetWorkByTitleRequest title = new GetWorkByTitleRequest();
		title.setTitle("Harry Potter");
		GetWorkByTitleResponse titleResponse = workPort.getWorkByTitle(title);
		List<WorkInfo> workListByTitle = titleResponse.getWorkInfo();
		System.out.println("-----GetWorkByTitle-----");
		for (int i = 0; i < workListByTitle.size(); i++) {
			System.out.println("Titre : " + workListByTitle.get(i).getTitle() + "\nAuteur : "
					+ workListByTitle.get(i).getAuthor() + "\nR�sum� : " + workListByTitle.get(i).getSummary()
					+ "\nAnn�e de parution : " + workListByTitle.get(i).getYearPublication());
		}
		//GETWORKBYDATE -->
		GetWorkByDateRequest date = new GetWorkByDateRequest();
		date.setDate(2018);
		GetWorkByDateResponse dateResponse = workPort.getWorkByDate(date);		
		List<WorkInfo> workListByDate = dateResponse.getWorkInfo();
		System.out.println("-----GetWorkByDate-----");
		for (int i = 0; i < workListByDate.size(); i++) {
			System.out.println("Titre : " + workListByDate.get(i).getTitle() + "\nAuteur : "
					+ workListByDate.get(i).getAuthor() + "\nR�sum� : " + workListByDate.get(i).getSummary()
					+ "\nAnn�e de parution : " + workListByDate.get(i).getYearPublication());
		}
		//GETALLWORK -->
		GetAllWorkRequest getAll = new GetAllWorkRequest();
		GetAllWorkResponse getAllResponse = workPort.getAllWork(getAll);
		List<WorkInfo> workList = getAllResponse.getWorkInfo();
		
		System.out.println("-----GetAllWork-----");
		for (int i = 0; i < workList.size(); i++) {
			System.out.println("Titre : " + workList.get(i).getTitle() + "\nAuteur : "
					+ workList.get(i).getAuthor() + "\nR�sum� : " + workList.get(i).getSummary()
					+ "\nAnn�e de parution : " + workList.get(i).getYearPublication());
			List<BookInfo> bookList = workList.get(i).getBookList();
			for (BookInfo book : bookList) {
				System.out.println(book.isAvailable());
			}
					
		}
//		//ADDWORK -->
//		AddWorkRequest addWork = new AddWorkRequest();
//		addWork.setAuthor("Veronica Roth");
//		addWork.setTitle("Divergente 1 : Divergent");
//		addWork.setSummary("Dans une ville d�truite enferm�e dans une cloture les hommes se divisent en quatres factions. Afin que r�gne la paix et l'ordre, chaque faction doit remplir son role.");
//		addWork.setYearPublication(2008);
//		addWork.setStockQuantity(2);
//		AddWorkResponse addWorkResponse = workPort.addWork(addWork);
//		WorkInfo addWorkInfo  = addWorkResponse.getWorkInfo();
//		System.out.println("-----AddWork-----");
//		System.out.println("Titre : " + addWorkInfo.getTitle() + "\nAuteur : "
//				+ addWorkInfo.getAuthor() + "\nR�sum� : " + addWorkInfo.getSummary()
//				+ "\nAnn�e de parution : " + addWorkInfo.getYearPublication());
		
	}

}
