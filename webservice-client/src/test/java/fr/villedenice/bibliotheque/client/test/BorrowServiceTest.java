package fr.villedenice.bibliotheque.client.test;

import java.util.List;

import fr.villedenice.bibliotheque.client.ws.BorrowInfo;
import fr.villedenice.bibliotheque.client.ws.GetAllBorrowRequest;
import fr.villedenice.bibliotheque.client.ws.GetAllBorrowResponse;
import fr.villedenice.bibliotheque.client.ws.SendMailRequest;
import fr.villedenice.bibliotheque.client.ws.SendMailResponse;
import fr.villedenice.bibliotheque.client.ws.WorkPort;
import fr.villedenice.bibliotheque.client.ws.WorkPortService;

public class BorrowServiceTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WorkPortService workPortService = new WorkPortService();
		WorkPort workPort = workPortService.getWorkPortSoap11();
		// STARTBORROW -->
//		StartBorrowRequest start = new StartBorrowRequest();
//		start.setCustomerId(1);
//		start.setWorkId(17);
//		StartBorrowResponse startResponse = workPort.startBorrow(start);
//		BorrowInfo borrowInfo = startResponse.getBorrowInfo();
//		System.out.println("-----StartBorrow-----");
//		System.out.println("Date d'emprunt : "+borrowInfo.getBorrowDate()+"\nDate de retour : "+borrowInfo.getLimitDate());

		GetAllBorrowRequest getAllRequest = new GetAllBorrowRequest();
		getAllRequest.setCustomerId(2);
		GetAllBorrowResponse getAllResponse = workPort.getAllBorrow(getAllRequest);
		List<BorrowInfo> borrowInfo = getAllResponse.getBorrowInfo();
		System.out.println("-----GetAllBorrow-----");
		for (int i = 0; i < borrowInfo.size(); i++) {
			System.out.println(borrowInfo.get(i).getBorrowDate());
		}
		SendMailRequest sendMailRequest = new SendMailRequest();
		SendMailResponse sendMailResponse = workPort.sendMail(sendMailRequest);
		System.out.println(sendMailResponse.getServiceStatus().getMessage());
		
	}

}
