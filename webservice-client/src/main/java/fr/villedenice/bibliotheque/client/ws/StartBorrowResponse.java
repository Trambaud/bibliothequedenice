
package fr.villedenice.bibliotheque.client.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceStatus" type="{http://www.villedenicebibliotheque.fr/work-ws}serviceStatus"/>
 *         &lt;element name="borrowInfo" type="{http://www.villedenicebibliotheque.fr/work-ws}borrowInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "serviceStatus",
    "borrowInfo"
})
@XmlRootElement(name = "startBorrowResponse")
public class StartBorrowResponse {

    @XmlElement(required = true)
    protected ServiceStatus serviceStatus;
    @XmlElement(required = true)
    protected BorrowInfo borrowInfo;

    /**
     * Obtient la valeur de la propriété serviceStatus.
     * 
     * @return
     *     possible object is
     *     {@link ServiceStatus }
     *     
     */
    public ServiceStatus getServiceStatus() {
        return serviceStatus;
    }

    /**
     * Définit la valeur de la propriété serviceStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceStatus }
     *     
     */
    public void setServiceStatus(ServiceStatus value) {
        this.serviceStatus = value;
    }

    /**
     * Obtient la valeur de la propriété borrowInfo.
     * 
     * @return
     *     possible object is
     *     {@link BorrowInfo }
     *     
     */
    public BorrowInfo getBorrowInfo() {
        return borrowInfo;
    }

    /**
     * Définit la valeur de la propriété borrowInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowInfo }
     *     
     */
    public void setBorrowInfo(BorrowInfo value) {
        this.borrowInfo = value;
    }

}
