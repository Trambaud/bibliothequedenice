
package fr.villedenice.bibliotheque.client.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="borrowInfo" type="{http://www.villedenicebibliotheque.fr/work-ws}borrowInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "borrowInfo"
})
@XmlRootElement(name = "getBorrowByIdResponse")
public class GetBorrowByIdResponse {

    @XmlElement(required = true)
    protected BorrowInfo borrowInfo;

    /**
     * Obtient la valeur de la propriété borrowInfo.
     * 
     * @return
     *     possible object is
     *     {@link BorrowInfo }
     *     
     */
    public BorrowInfo getBorrowInfo() {
        return borrowInfo;
    }

    /**
     * Définit la valeur de la propriété borrowInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link BorrowInfo }
     *     
     */
    public void setBorrowInfo(BorrowInfo value) {
        this.borrowInfo = value;
    }

}
