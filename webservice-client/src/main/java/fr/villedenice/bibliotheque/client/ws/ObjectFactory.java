
package fr.villedenice.bibliotheque.client.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.villedenice.bibliotheque.client.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.villedenice.bibliotheque.client.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetWorkByTitleRequest }
     * 
     */
    public GetWorkByTitleRequest createGetWorkByTitleRequest() {
        return new GetWorkByTitleRequest();
    }

    /**
     * Create an instance of {@link EndBorrowResponse }
     * 
     */
    public EndBorrowResponse createEndBorrowResponse() {
        return new EndBorrowResponse();
    }

    /**
     * Create an instance of {@link ServiceStatus }
     * 
     */
    public ServiceStatus createServiceStatus() {
        return new ServiceStatus();
    }

    /**
     * Create an instance of {@link GetBorrowByIdResponse }
     * 
     */
    public GetBorrowByIdResponse createGetBorrowByIdResponse() {
        return new GetBorrowByIdResponse();
    }

    /**
     * Create an instance of {@link BorrowInfo }
     * 
     */
    public BorrowInfo createBorrowInfo() {
        return new BorrowInfo();
    }

    /**
     * Create an instance of {@link GetWorkByIdRequest }
     * 
     */
    public GetWorkByIdRequest createGetWorkByIdRequest() {
        return new GetWorkByIdRequest();
    }

    /**
     * Create an instance of {@link SendMailRequest }
     * 
     */
    public SendMailRequest createSendMailRequest() {
        return new SendMailRequest();
    }

    /**
     * Create an instance of {@link EndBorrowRequest }
     * 
     */
    public EndBorrowRequest createEndBorrowRequest() {
        return new EndBorrowRequest();
    }

    /**
     * Create an instance of {@link GetWorkByFilterResponse }
     * 
     */
    public GetWorkByFilterResponse createGetWorkByFilterResponse() {
        return new GetWorkByFilterResponse();
    }

    /**
     * Create an instance of {@link WorkInfo }
     * 
     */
    public WorkInfo createWorkInfo() {
        return new WorkInfo();
    }

    /**
     * Create an instance of {@link AddWorkRequest }
     * 
     */
    public AddWorkRequest createAddWorkRequest() {
        return new AddWorkRequest();
    }

    /**
     * Create an instance of {@link LogInResponse }
     * 
     */
    public LogInResponse createLogInResponse() {
        return new LogInResponse();
    }

    /**
     * Create an instance of {@link CustomerInfo }
     * 
     */
    public CustomerInfo createCustomerInfo() {
        return new CustomerInfo();
    }

    /**
     * Create an instance of {@link StartBorrowResponse }
     * 
     */
    public StartBorrowResponse createStartBorrowResponse() {
        return new StartBorrowResponse();
    }

    /**
     * Create an instance of {@link LogOutRequest }
     * 
     */
    public LogOutRequest createLogOutRequest() {
        return new LogOutRequest();
    }

    /**
     * Create an instance of {@link GetAllBorrowRequest }
     * 
     */
    public GetAllBorrowRequest createGetAllBorrowRequest() {
        return new GetAllBorrowRequest();
    }

    /**
     * Create an instance of {@link LogOutResponse }
     * 
     */
    public LogOutResponse createLogOutResponse() {
        return new LogOutResponse();
    }

    /**
     * Create an instance of {@link GetAllWorkRequest }
     * 
     */
    public GetAllWorkRequest createGetAllWorkRequest() {
        return new GetAllWorkRequest();
    }

    /**
     * Create an instance of {@link GetWorkByAuthorResponse }
     * 
     */
    public GetWorkByAuthorResponse createGetWorkByAuthorResponse() {
        return new GetWorkByAuthorResponse();
    }

    /**
     * Create an instance of {@link UpdateWorkRequest }
     * 
     */
    public UpdateWorkRequest createUpdateWorkRequest() {
        return new UpdateWorkRequest();
    }

    /**
     * Create an instance of {@link ExtendBorrowRequest }
     * 
     */
    public ExtendBorrowRequest createExtendBorrowRequest() {
        return new ExtendBorrowRequest();
    }

    /**
     * Create an instance of {@link GetWorkByDateRequest }
     * 
     */
    public GetWorkByDateRequest createGetWorkByDateRequest() {
        return new GetWorkByDateRequest();
    }

    /**
     * Create an instance of {@link GetWorkByDateResponse }
     * 
     */
    public GetWorkByDateResponse createGetWorkByDateResponse() {
        return new GetWorkByDateResponse();
    }

    /**
     * Create an instance of {@link GetWorkByAuthorRequest }
     * 
     */
    public GetWorkByAuthorRequest createGetWorkByAuthorRequest() {
        return new GetWorkByAuthorRequest();
    }

    /**
     * Create an instance of {@link StartBorrowRequest }
     * 
     */
    public StartBorrowRequest createStartBorrowRequest() {
        return new StartBorrowRequest();
    }

    /**
     * Create an instance of {@link GetCustomerByMailResponse }
     * 
     */
    public GetCustomerByMailResponse createGetCustomerByMailResponse() {
        return new GetCustomerByMailResponse();
    }

    /**
     * Create an instance of {@link GetWorkByIdResponse }
     * 
     */
    public GetWorkByIdResponse createGetWorkByIdResponse() {
        return new GetWorkByIdResponse();
    }

    /**
     * Create an instance of {@link AddWorkResponse }
     * 
     */
    public AddWorkResponse createAddWorkResponse() {
        return new AddWorkResponse();
    }

    /**
     * Create an instance of {@link UpdateWorkResponse }
     * 
     */
    public UpdateWorkResponse createUpdateWorkResponse() {
        return new UpdateWorkResponse();
    }

    /**
     * Create an instance of {@link LogInRequest }
     * 
     */
    public LogInRequest createLogInRequest() {
        return new LogInRequest();
    }

    /**
     * Create an instance of {@link ExtendBorrowResponse }
     * 
     */
    public ExtendBorrowResponse createExtendBorrowResponse() {
        return new ExtendBorrowResponse();
    }

    /**
     * Create an instance of {@link GetWorkByTitleResponse }
     * 
     */
    public GetWorkByTitleResponse createGetWorkByTitleResponse() {
        return new GetWorkByTitleResponse();
    }

    /**
     * Create an instance of {@link GetBorrowByIdRequest }
     * 
     */
    public GetBorrowByIdRequest createGetBorrowByIdRequest() {
        return new GetBorrowByIdRequest();
    }

    /**
     * Create an instance of {@link GetAllBorrowResponse }
     * 
     */
    public GetAllBorrowResponse createGetAllBorrowResponse() {
        return new GetAllBorrowResponse();
    }

    /**
     * Create an instance of {@link GetCustomerByMailRequest }
     * 
     */
    public GetCustomerByMailRequest createGetCustomerByMailRequest() {
        return new GetCustomerByMailRequest();
    }

    /**
     * Create an instance of {@link GetAllWorkResponse }
     * 
     */
    public GetAllWorkResponse createGetAllWorkResponse() {
        return new GetAllWorkResponse();
    }

    /**
     * Create an instance of {@link SendMailResponse }
     * 
     */
    public SendMailResponse createSendMailResponse() {
        return new SendMailResponse();
    }

    /**
     * Create an instance of {@link GetWorkByFilterRequest }
     * 
     */
    public GetWorkByFilterRequest createGetWorkByFilterRequest() {
        return new GetWorkByFilterRequest();
    }

    /**
     * Create an instance of {@link BookInfo }
     * 
     */
    public BookInfo createBookInfo() {
        return new BookInfo();
    }

    /**
     * Create an instance of {@link BookType }
     * 
     */
    public BookType createBookType() {
        return new BookType();
    }

}
