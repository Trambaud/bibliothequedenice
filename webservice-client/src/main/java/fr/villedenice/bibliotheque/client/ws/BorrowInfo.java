
package fr.villedenice.bibliotheque.client.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour borrowInfo complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="borrowInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="borrowId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="borrowDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="limitDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="book" type="{http://www.villedenicebibliotheque.fr/work-ws}bookInfo"/>
 *         &lt;element name="customer" type="{http://www.villedenicebibliotheque.fr/work-ws}customerInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "borrowInfo", propOrder = {
    "borrowId",
    "status",
    "borrowDate",
    "limitDate",
    "book",
    "customer"
})
public class BorrowInfo {

    protected int borrowId;
    protected int status;
    @XmlElement(required = true)
    protected String borrowDate;
    @XmlElement(required = true)
    protected String limitDate;
    @XmlElement(required = true)
    protected BookInfo book;
    @XmlElement(required = true)
    protected CustomerInfo customer;

    /**
     * Obtient la valeur de la propriété borrowId.
     * 
     */
    public int getBorrowId() {
        return borrowId;
    }

    /**
     * Définit la valeur de la propriété borrowId.
     * 
     */
    public void setBorrowId(int value) {
        this.borrowId = value;
    }

    /**
     * Obtient la valeur de la propriété status.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Définit la valeur de la propriété status.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Obtient la valeur de la propriété borrowDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBorrowDate() {
        return borrowDate;
    }

    /**
     * Définit la valeur de la propriété borrowDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBorrowDate(String value) {
        this.borrowDate = value;
    }

    /**
     * Obtient la valeur de la propriété limitDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitDate() {
        return limitDate;
    }

    /**
     * Définit la valeur de la propriété limitDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitDate(String value) {
        this.limitDate = value;
    }

    /**
     * Obtient la valeur de la propriété book.
     * 
     * @return
     *     possible object is
     *     {@link BookInfo }
     *     
     */
    public BookInfo getBook() {
        return book;
    }

    /**
     * Définit la valeur de la propriété book.
     * 
     * @param value
     *     allowed object is
     *     {@link BookInfo }
     *     
     */
    public void setBook(BookInfo value) {
        this.book = value;
    }

    /**
     * Obtient la valeur de la propriété customer.
     * 
     * @return
     *     possible object is
     *     {@link CustomerInfo }
     *     
     */
    public CustomerInfo getCustomer() {
        return customer;
    }

    /**
     * Définit la valeur de la propriété customer.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerInfo }
     *     
     */
    public void setCustomer(CustomerInfo value) {
        this.customer = value;
    }

}
