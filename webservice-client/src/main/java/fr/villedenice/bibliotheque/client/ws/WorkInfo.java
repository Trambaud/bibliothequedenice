
package fr.villedenice.bibliotheque.client.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour workInfo complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="workInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="workId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="author" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="yearPublication" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="summary" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stockQuantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="type" type="{http://www.villedenicebibliotheque.fr/work-ws}bookType"/>
 *         &lt;element name="bookList" type="{http://www.villedenicebibliotheque.fr/work-ws}bookInfo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "workInfo", propOrder = {
    "workId",
    "title",
    "author",
    "yearPublication",
    "summary",
    "stockQuantity",
    "type",
    "bookList"
})
public class WorkInfo {

    protected int workId;
    @XmlElement(required = true)
    protected String title;
    @XmlElement(required = true)
    protected String author;
    protected int yearPublication;
    @XmlElement(required = true)
    protected String summary;
    protected int stockQuantity;
    @XmlElement(required = true)
    protected BookType type;
    @XmlElement(required = true)
    protected List<BookInfo> bookList;

    /**
     * Obtient la valeur de la propriété workId.
     * 
     */
    public int getWorkId() {
        return workId;
    }

    /**
     * Définit la valeur de la propriété workId.
     * 
     */
    public void setWorkId(int value) {
        this.workId = value;
    }

    /**
     * Obtient la valeur de la propriété title.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Définit la valeur de la propriété title.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Obtient la valeur de la propriété author.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Définit la valeur de la propriété author.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthor(String value) {
        this.author = value;
    }

    /**
     * Obtient la valeur de la propriété yearPublication.
     * 
     */
    public int getYearPublication() {
        return yearPublication;
    }

    /**
     * Définit la valeur de la propriété yearPublication.
     * 
     */
    public void setYearPublication(int value) {
        this.yearPublication = value;
    }

    /**
     * Obtient la valeur de la propriété summary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Définit la valeur de la propriété summary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummary(String value) {
        this.summary = value;
    }

    /**
     * Obtient la valeur de la propriété stockQuantity.
     * 
     */
    public int getStockQuantity() {
        return stockQuantity;
    }

    /**
     * Définit la valeur de la propriété stockQuantity.
     * 
     */
    public void setStockQuantity(int value) {
        this.stockQuantity = value;
    }

    /**
     * Obtient la valeur de la propriété type.
     * 
     * @return
     *     possible object is
     *     {@link BookType }
     *     
     */
    public BookType getType() {
        return type;
    }

    /**
     * Définit la valeur de la propriété type.
     * 
     * @param value
     *     allowed object is
     *     {@link BookType }
     *     
     */
    public void setType(BookType value) {
        this.type = value;
    }

    /**
     * Gets the value of the bookList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookInfo }
     * 
     * 
     */
    public List<BookInfo> getBookList() {
        if (bookList == null) {
            bookList = new ArrayList<BookInfo>();
        }
        return this.bookList;
    }

}
