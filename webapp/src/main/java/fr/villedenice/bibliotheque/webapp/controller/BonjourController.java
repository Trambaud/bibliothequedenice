package fr.villedenice.bibliotheque.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.villedenice.bibliotheque.client.ws.CustomerInfo;
import fr.villedenice.bibliotheque.client.ws.LogInRequest;
import fr.villedenice.bibliotheque.client.ws.LogInResponse;
import fr.villedenice.bibliotheque.client.ws.WorkPort;
import fr.villedenice.bibliotheque.client.ws.WorkPortService;

@Controller
public class BonjourController {
	WorkPortService workPortService = new WorkPortService();
	WorkPort workPort = workPortService.getWorkPortSoap11();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView afficher() {
		ModelAndView modelAndView = new ModelAndView("logIn");
	
		return modelAndView;
	}

	

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	
	public ModelAndView logIn(HttpServletRequest request) {
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		LogInRequest logRequest = new LogInRequest();
		logRequest.setMail(login);
		logRequest.setPassword(password);
		LogInResponse response = workPort.logIn(logRequest);
		if (response.getCustomerInfo().isConnected()) {
			ModelAndView success = new ModelAndView("accueil");
			HttpSession session = request.getSession();
			CustomerInfo customerInfo = response.getCustomerInfo();
			session.setAttribute("user", customerInfo);
			success.addObject("user", customerInfo.getFirstName());
			return success;
		} else {
			ModelAndView error = new ModelAndView();
			error.addObject("messageDeRetour", "Utilisateur ou mot de passe errone.");
			error.setViewName("redirect:/");
			
			return error;
		}

	}
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
public ModelAndView logOut(HttpServletRequest request) {
		request.getSession().invalidate();
		ModelAndView accueil = new ModelAndView();
		accueil.setViewName("redirect:/");
		return accueil;
	}
}
