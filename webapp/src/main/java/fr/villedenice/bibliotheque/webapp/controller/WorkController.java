package fr.villedenice.bibliotheque.webapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.villedenice.bibliotheque.client.ws.GetAllWorkRequest;
import fr.villedenice.bibliotheque.client.ws.GetAllWorkResponse;
import fr.villedenice.bibliotheque.client.ws.GetWorkByAuthorRequest;
import fr.villedenice.bibliotheque.client.ws.GetWorkByAuthorResponse;
import fr.villedenice.bibliotheque.client.ws.GetWorkByDateRequest;
import fr.villedenice.bibliotheque.client.ws.GetWorkByDateResponse;
import fr.villedenice.bibliotheque.client.ws.GetWorkByFilterRequest;
import fr.villedenice.bibliotheque.client.ws.GetWorkByFilterResponse;
import fr.villedenice.bibliotheque.client.ws.GetWorkByTitleRequest;
import fr.villedenice.bibliotheque.client.ws.GetWorkByTitleResponse;
import fr.villedenice.bibliotheque.client.ws.WorkInfo;
import fr.villedenice.bibliotheque.client.ws.WorkPort;
import fr.villedenice.bibliotheque.client.ws.WorkPortService;
@Controller
public class WorkController {
	
	WorkPortService workPortService = new WorkPortService();
	WorkPort workPort = workPortService.getWorkPortSoap11();

    @RequestMapping(value="/allwork", method = RequestMethod.GET)
    public ModelAndView afficher() {
    	GetAllWorkRequest getAll = new GetAllWorkRequest();
		GetAllWorkResponse getAllResponse = workPort.getAllWork(getAll);
		List<WorkInfo> workList = getAllResponse.getWorkInfo();
        ModelAndView modelWork = new ModelAndView("allWork");
       modelWork.addObject("allWork", workList);
        return modelWork;
}
    @RequestMapping(value="/search", method = RequestMethod.GET)
    public ModelAndView search() {
    	ModelAndView modelSearch = new ModelAndView("search");
    	
		return modelSearch;
    }
    @RequestMapping(value="/byAuthor", method = RequestMethod.POST)
    public ModelAndView byAuthor(HttpServletRequest request) {
    	ModelAndView modelWork = new ModelAndView("allWork");
    	GetWorkByAuthorRequest getByAuthorRequest = new GetWorkByAuthorRequest();
    	getByAuthorRequest.setAuthor(request.getParameter("author"));
    	GetWorkByAuthorResponse getByAuthorResponse = workPort.getWorkByAuthor(getByAuthorRequest);
    	List<WorkInfo> listByAuthor = getByAuthorResponse.getWorkInfo();
    	modelWork.addObject("allWork", listByAuthor);  	
    	modelWork.addObject("author", "Recherche pour l'auteur :"+request.getParameter("author"));
    	return modelWork;
    }
    
    @RequestMapping(value="/byTitle", method = RequestMethod.POST)
    public ModelAndView byTitle(HttpServletRequest request) {
    	ModelAndView modelWork = new ModelAndView("allWork");
    	GetWorkByTitleRequest getByTitleRequest = new GetWorkByTitleRequest();
    	getByTitleRequest.setTitle(request.getParameter("title"));
    	GetWorkByTitleResponse getByTitleResponse = workPort.getWorkByTitle(getByTitleRequest);
    	List<WorkInfo> listByTitle = getByTitleResponse.getWorkInfo();
    	modelWork.addObject("allWork", listByTitle);  	
    	modelWork.addObject("title", "Recherche pour le titre :"+request.getParameter("title"));
    	return modelWork;
    }
    @RequestMapping(value="/byYear", method = RequestMethod.POST)
    public ModelAndView byYear(HttpServletRequest request) {
    	ModelAndView modelWork = new ModelAndView("allWork");
    	GetWorkByDateRequest getByYearRequest = new GetWorkByDateRequest();
    	int date = Integer.parseInt(request.getParameter("year"));
    	getByYearRequest.setDate(date);
    	GetWorkByDateResponse getByYearResponse = workPort.getWorkByDate(getByYearRequest);
    	List<WorkInfo> listByYear = getByYearResponse.getWorkInfo();
    	modelWork.addObject("allWork", listByYear);  	
    	modelWork.addObject("year", "Recherche pour l'année :"+request.getParameter("year"));
    	return modelWork;
    }
    @RequestMapping(value="/searchWork", method = RequestMethod.POST)
    public ModelAndView searchWork(HttpServletRequest request) {
    	ModelAndView modelWork = new ModelAndView("allWork");
    	GetWorkByFilterRequest getByFilterRequest = new GetWorkByFilterRequest();
    	WorkInfo workInfo = new WorkInfo();
    	String author = request.getParameter("author");
    	String title = request.getParameter("title");
    	workInfo.setAuthor(author);
    	workInfo.setTitle(title);
    	if (!request.getParameter("year").isEmpty()) {
    		
    		int year = Integer.parseInt(request.getParameter("year"));
    		workInfo.setYearPublication(year);
    	}
    
    	getByFilterRequest.setWorkInfo(workInfo);
    	GetWorkByFilterResponse getByFilterResponse = workPort.getWorkByFilter(getByFilterRequest);
    	List<WorkInfo> workInfoList = getByFilterResponse.getWorkInfo();
    	modelWork.addObject("allWork", workInfoList);
    	
    	return modelWork;
    }
    
    
    
}
