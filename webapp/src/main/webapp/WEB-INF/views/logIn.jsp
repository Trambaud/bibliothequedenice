<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="<c:url value="/resources/bootstrap/css/bootstrap.css" />"
	rel="stylesheet">
<head>
<title>Bibliothèque de Nice - Connexion</title>
</head>
<body>
	<div class="container">
		<header class="page-header">
		<h2>Bibliothèque de Nice</h2>
		</header>

		<section class="row">
		<div class="col-lg-offset-4 col-lg-8">
			<p>Afin d'accéder aux services de la bibliothèque, une connexion
				est requise.</p>
		</div>
		</section>
		<section class="row">
		<div class="col-lg-offset-4 col-lg-8">
			<form:form method="post" action="login">
				<div class="form-group">
					<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="saisissez votre login" name="login">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="saisissez votre mot de passe"
						name="password">
				</div>
					<input type="submit" class="btn btn-info"/>
				
				
	<% 
		if (request.getParameter("messageDeRetour")!=null){
			String parametre = request.getParameter("messageDeRetour");
			out.println(parametre);
				}
	%>

				
			</form:form>
		</div>
		</section>
	</div>
</body>
</html>