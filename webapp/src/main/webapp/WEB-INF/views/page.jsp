<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="<c:url value="/resources/bootstrap/css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="/resources/bootstrap/css/custom-css.css" />" rel="stylesheet">
<title><spring:message code="titre.default" /></title>
</head>
<body>


<div class="container" >

<nav class="navbar navbar-nav navbar-light bg-light">
  <div class="navbar-header">
    <a class="navbar-brand" href="#">Bibliothèque de Nice</a>
  </div>
  <ul class="navbar navbar-nav navbar-light bg-light">
    <li class="nav-item mx-3">
    <c:url value="/allwork" var="url" /> <a class="nav-link" href="${url}"><spring:message code="titre.allwork" /></a>
    </li>
    <li class="nav-item mx-3">
    <c:url value="/search" var="url" /> <a class="nav-link" href="${url}"><spring:message code="titre.search" />	</a>
    </li>
    <li class="nav-item mx-3">
    <c:url value="/borrowList" var="url" /> <a  class="nav-link" href="${url}"> <spring:message code="titre.borrowList" /></a>
    </li>
    <li class="nav-item mx-3">
    <c:url value="/logout" var="url" /> <a  class="btn btn-info" href="${url}"> Se déconnecter</a>
    </li>
  </ul>
</nav>

<div class="row">

		

		
	<section class="col-lg-12">	
	<tiles:insertAttribute name="principal" />
	</section>		
</div>

</div>	
</body>
</html>
